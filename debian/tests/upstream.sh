#!/bin/bash

set -e
set -u

testdir=test
cp -R $testdir $AUTOPKGTEST_TMP
cd $AUTOPKGTEST_TMP
rm ${testdir}/test_helpers.py

for py3ver in $(py3versions -vs)
do
    echo "Running tests with Python ${py3ver}."
    /usr/bin/python${py3ver} -B -m pytest $testdir
    rm -rf .pytest_cache
    echo "**********"
done
